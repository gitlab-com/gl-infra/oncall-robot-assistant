package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/urfave/cli/v2"
	"github.com/urfave/cli/v2/altsrc"
	"gitlab.com/gitlab-com/gl-infra/oncall-robot-assistant/config"
	"gitlab.com/gitlab-com/gl-infra/oncall-robot-assistant/oncall"
)

func main() {

	flags := []cli.Flag{
		altsrc.NewBoolFlag(&cli.BoolFlag{
			Name:  "debug",
			Usage: "Print the issue instead of creating it.",
		}),
		altsrc.NewStringFlag(&cli.StringFlag{
			Name:  "config",
			Usage: "Point to the config file",
			// FilePath: os.Getenv(),
			EnvVars: []string{"CONFIG_FILE", "CONFIG"},
		}),
		// TODO add flag to link and close old issue
	}

	app := &cli.App{
		Name:  "Oncall Robot Assistant",
		Usage: "Generate an issue with Announcements and Oncall Tables and Statistics",
		Commands: []*cli.Command{
			{
				Name:  "add-oncall",
				Usage: "add the oncall statistic to the issue description",
				Flags: flags,
				Action: func(ctx *cli.Context) error {
					updateOnCallReport(ctx)
					return nil
				},
			},
		},
		// Before: altsrc.InitInputSourceWithContext(flags, altsrc.NewYamlSourceFromFlagFunc("load")),
		Flags: flags,
		Action: func(ctx *cli.Context) error {
			generateIssue(ctx)
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func loadConfig(input string, debug bool) config.Config {
	var path config.Path
	if len(input) > 0 {
		path = config.FromFlag(input)
	}
	if !path.Exists() {
		path = config.FromLocalDir()
	}
	if !path.Exists() {
		path = config.FromHomeDir()
	}
	if !path.Exists() {
		log.Fatalln("Unable to locate a config file!")
	}

	config, err := config.ReadConfig(path, debug)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("Using config file at", path)
	return *config
}

func configFromContext(ctx *cli.Context) *config.Config {
	var cfg config.Config
	var debug bool
	if ctx.Bool("debug") {
		debug = true
	}
	path := ctx.String("config")
	cfg = loadConfig(path, debug)
	return &cfg
}

func generateIssue(ctx *cli.Context) {
	cfg := configFromContext(ctx)
	report := oncall.NewReport(cfg, oncall.ReportOptions{})
	title := "Weekly Reliability (SRE) Team Newsletter – On-call Period: " +
		time.Now().UTC().Format("2006-01-02") +
		" - " + time.Now().UTC().AddDate(0, 0, 7).Format("2006-01-02")
	issue := report.CreateReportIssue(title)
	if issue != nil {
		fmt.Println("Created issue: ", issue.WebURL)
	}
}

func updateOnCallReport(ctx *cli.Context) {
	cfg := configFromContext(ctx)
	report := oncall.NewReport(cfg, oncall.ReportOptions{})
	issue := report.Helpers.GitLab.GetIssuesWeeklyNewsletter()
	report.UpdateIssueDescriptionWithOnCall(issue)
}
