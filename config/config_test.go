package config

import (
	"fmt"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestReadConfig(t *testing.T) {
	path := Path{Directory: ".", File: "invalid-cfg.json"}
	_, err := ReadConfig(path, false)
	if err == nil {
		t.Fatalf("could not read configuration file %s", path)
	}

	want := "stat ./invalid-cfg.json: no such file or directory"
	got := err.Error()
	if got != want {
		t.Fatalf("wrong configuration error, got %s; expected %s", got, want)
	}
}

func TestParseConfigWrong(t *testing.T) {
	_, err := ParseConfig(strings.NewReader("invalid yaml"))
	expected := "could not parse configuration yaml: yaml: unmarshal errors:\n  line 1: cannot unmarshal !!str `invalid...` into config.Config"
	if fmt.Sprintf("%s", err) != expected {
		t.Fatalf("unexpected error, got %s", err)
	}
}

func TestParseConfigCorrectly(t *testing.T) {
	f, err := os.Open("../oncall-settings-example.yaml")

	if err != nil {
		t.Fatalf("failed to open the configuration file: %s", err)
	}

	defer f.Close()

	c, err := ParseConfig(f)
	if err != nil {
		t.Fatalf("failed to parse the configuration: %s", err)
	}
	assertEquals(t, "gitlab token", "", c.APITokens.GitLab)
	assertEquals(t, "pagerduty token", "", c.APITokens.PagerDuty)
}

func assertEquals(t *testing.T, name string, expected, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("%s is not as expected: %+v; got %+v", name, expected, actual)
	}
}
