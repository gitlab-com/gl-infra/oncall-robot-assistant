package config

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

// DefaultFileName for configuration YAML file
const DefaultFileName = ".oncall-settings.yaml"

// Path finds the file to read YAML from
type Path struct {
	Directory string
	File      string
}

func (p Path) String() string {
	return fmt.Sprintf("%s/%s", p.Directory, p.File)
}

// Exists confirms the file exists
func (p Path) Exists() bool {
	_, err := os.Stat(fmt.Sprint(p))
	if err != nil || p == (Path{}) {
		return false
	}
	return true
}

// WeeklyOps for performance graphs
type WeeklyOps struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url"`
}

// PagerDutySchedule for pd rotations
type PagerDutySchedule struct {
	Name string `yaml:"name"`
	ID   string `yaml:"id"`
}

// PagerDuty for PD schedules
type PagerDuty struct {
	ServiceID         string              `yaml:"service_id"`
	Schedules         []PagerDutySchedule `yaml:"schedules"`
	PrimarySchedule   string              `yaml:"primary"`
	SecondarySchedule string              `yaml:"secondary"`
}

// Project has data for a GitLab project
type Project struct {
	ID int `yaml:"id"`
}

// Projects for project ids
type Projects struct {
	Infrastructure Project `yaml:"infrastructure"`
	Production     Project `yaml:"production"`
	ReportProject  Project `yaml:"report_project"`
}

// APITokens for api tokens
type APITokens struct {
	GitLab    string `yaml:"gitlab"`
	GitLabDev string `yaml:"gitlab_dev"`
	Grafana   string `yaml:"grafana"`
	PagerDuty string `yaml:"pager_duty"`
}

// Config for holding report settings
type Config struct {
	APITokens   APITokens   `yaml:"api_tokens"`
	Projects    Projects    `yaml:"projects"`
	DayOffset   int         `yaml:"day_offset"`
	OncallLabel string      `yaml:"oncall_label"`
	PagerDuty   PagerDuty   `yaml:"pagerduty"`
	WeeklyOps   []WeeklyOps `yaml:"weekly_ops"`
	Debug       bool
	Path        Path
}

// ParseConfig parses the yaml configuration from a reader
func ParseConfig(r io.Reader) (Config, error) {
	configBytes, err := io.ReadAll(r)
	config := Config{}
	if err != nil {
		return config, fmt.Errorf("could not read configuration: %s", err)
	}
	err = yaml.Unmarshal(configBytes, &config)
	if err != nil {
		return config, fmt.Errorf("could not parse configuration yaml: %s", err)
	}
	return config, nil
}

// ReadConfig will attempt to read the different configuration
// parameters from a yaml formatted file.
func ReadConfig(p Path, d bool) (*Config, error) {
	var cfg Config
	if _, err := os.Stat(p.String()); os.IsNotExist(err) {
		return nil, errors.New(err.Error())
	}
	content, err := os.ReadFile(p.String())
	if err != nil {
		return nil, errors.New(err.Error())
	}
	err = yaml.Unmarshal(content, &cfg)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	if cfg.Projects.Infrastructure.ID == 0 {
		return nil, errors.New("ERROR: It looks like you have an old settings file, please update using oncall-settings-example.yaml")
	}
	if cfg.Projects.Production.ID == 0 {
		return nil, errors.New("ERROR: It looks like you have an old settings file, you are missing the production project ID. please update using oncall-settings-example.yaml")
	}

	if cfg.APITokens.GitLab == "" {
		cfg.APITokens.GitLab = os.Getenv("GITLAB_API_TOKEN")
	}
	if cfg.APITokens.Grafana == "" {
		cfg.APITokens.Grafana = os.Getenv("GRAFANA_API_KEY")
	}
	if cfg.APITokens.PagerDuty == "" {
		cfg.APITokens.PagerDuty = os.Getenv("PAGER_DUTY_TOKEN")
	}
	if cfg.APITokens.GitLab == "" || cfg.APITokens.Grafana == "" || cfg.APITokens.PagerDuty == "" {
		return nil, errors.New("ERROR: Please configure api tokens")
	}
	cfg.Debug = d
	cfg.Path = p
	return &cfg, nil
}

// FromFlag generates a Path from config flag input
func FromFlag(input string) Path {
	var path Path
	var directory, file string

	if strPath, err := filepath.Abs(input); err == nil {
		fileInfo, err := os.Stat(strPath)
		if err == nil && fileInfo.IsDir() {
			directory = strPath
			file = DefaultFileName
		} else if err == nil {
			directory = filepath.Dir(strPath)
			file = filepath.Base(strPath)
		} else {
			log.Fatalf("No config file located: (%s)", err)
		}
		return Path{
			Directory: directory,
			File:      file,
		}
	}
	return path
}

// FromLocalDir generates a Path from the directory where the command is run
func FromLocalDir() Path {
	var path Path
	if wd, err := os.Getwd(); err == nil {
		path = Path{
			Directory: wd,
			File:      DefaultFileName,
		}
	}
	return path
}

// FromHomeDir generates a Path from the $HOME environment variable of the calling useer
func FromHomeDir() Path {
	path := Path{
		Directory: os.Getenv("HOME"),
		File:      DefaultFileName,
	}
	return path
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
