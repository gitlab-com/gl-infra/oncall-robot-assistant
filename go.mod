module gitlab.com/gitlab-com/gl-infra/oncall-robot-assistant

go 1.23.4

require (
	github.com/GeertJohan/go.rice v1.0.3
	github.com/PagerDuty/go-pagerduty v1.8.0
	github.com/urfave/cli/v2 v2.27.5
	gitlab.com/gitlab-org/api/client-go v0.123.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/daaku/go.zipexe v1.0.2 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.25.0 // indirect
	golang.org/x/time v0.9.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.36.0 // indirect
)
