# Oncall Robot Assistant

This is a utility for helping to automate on-call reporting
tasks and the Reliability (SRE) Team's weekly meeting agenda.

It's main purpose to generate weekly reports with a breakdown
of PagerDuty incidents and ~incident issues alongside an
agenda template for discussions during weekly Reliability Team
staff conversations. See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10505.

# How to use

* `go install`
* `go build`
* Copy `oncall-settings-example.yaml` to `~/.oncall-settings.yaml` and update the pagerduty, grafana and gitlab tokens. These are located in 1Password in the production vault, grep for `oncall-robot`.
* Run `oncall-robot-assistant` to generate the report.

This will create an issue link, modify the issue as you see fit
