package oncall

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	gitlab "gitlab.com/gitlab-org/api/client-go"
)

// GitLabHelper api helper
type GitLabHelper struct {
	client          *gitlab.Client
	apiToken        string
	projectID       int
	projectIDProd   int
	projectIDReport int
	dayOffset       int
}

// GitLabHelperOptions options for the api helper
type GitLabHelperOptions struct {
	APIToken        string
	ProjectID       int
	ProjectIDProd   int
	ProjectIDReport int
	DayOffset       int
}

// NewGitLabHelper creates a new api helper
func NewGitLabHelper(opts GitLabHelperOptions) *GitLabHelper {
	client, err := newGitLabClient(opts.APIToken)
	if client == nil {
		panic(err)
	}
	return &GitLabHelper{
		client:          client,
		apiToken:        opts.APIToken,
		projectID:       opts.ProjectID,
		projectIDProd:   opts.ProjectIDProd,
		projectIDReport: opts.ProjectIDReport,
		dayOffset:       opts.DayOffset,
	}
}

func newGitLabClient(apiToken string) (*gitlab.Client, error) {
	return gitlab.NewClient(apiToken)
}

// GetMergeRequestsByProject get merge requests
func (g *GitLabHelper) GetMergeRequestsByProject() []*gitlab.MergeRequest {
	opts := gitlab.ListProjectMergeRequestsOptions{}
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval

	mrs, _, err := g.client.MergeRequests.ListProjectMergeRequests(g.projectID, &opts)
	if err != nil {
		panic(err)
	}
	return mrs
}

// CreateIssue creates an issue
func (g *GitLabHelper) CreateIssue(title string, desc string, projID int) *gitlab.Issue {
	opts := gitlab.CreateIssueOptions{}
	opts.Title = &title
	opts.Description = &desc
	// TODO - move these assignees options to the settings YAML
	opts.AssigneeIDs = &[]int{4059254, 19859972, 2222520}
	issue, _, err := g.client.Issues.CreateIssue(projID, &opts)
	if err != nil {
		log.Fatalf("Unable to create issue in GitLab project %d: %s", projID, err.Error())
	}
	return issue
}

// GetIssuesOpenedDuringShift gets opened issues during shift
func (g *GitLabHelper) GetIssuesOpenedDuringShift() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectID, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Setting next page to ", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenedDuringShiftProd gets opened issues during shift for production issues
func (g *GitLabHelper) GetIssuesOpenedDuringShiftProd() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	dayInterval := time.Now().UTC().AddDate(0, 0, -g.dayOffset)
	opts.CreatedAfter = &dayInterval
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectIDProd, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Setting next page to ", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenAll gets all open issues
func (g *GitLabHelper) GetIssuesOpenAll() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	state := "opened"
	opts.State = &state
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectID, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Getting next page from response", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesOpenAllProd gets all open issues in production
func (g *GitLabHelper) GetIssuesOpenAllProd() []*gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 100
	state := "opened"
	opts.State = &state
	allIssues := make([]*gitlab.Issue, 0)
	for {
		issues, r, err := g.client.Issues.ListProjectIssues(g.projectIDProd, &opts)
		if err != nil {
			panic(err)
		}
		for _, issue := range issues {
			allIssues = append(allIssues, issue)
		}
		if r.NextPage == 0 {
			break
		}
		log.Println("Getting next page from response", r.NextPage)
		opts.ListOptions.Page = r.NextPage
	}
	return allIssues
}

// GetIssuesWeeklyNewsletter gets all the open issues with label "Reliability-Weekly-Newsletter"
func (g *GitLabHelper) GetIssuesWeeklyNewsletter() *gitlab.Issue {
	var opts gitlab.ListProjectIssuesOptions
	opts.ListOptions.PerPage = 1
	opts.Labels = &gitlab.LabelOptions{"Reliability-Team-Newsletter"} // TODO label from config
	state := "opened"
	opts.State = &state
	orderBy := "created_at"
	opts.OrderBy = &orderBy
	sort := "desc"
	opts.Sort = &sort

	issues, _, err := g.client.Issues.ListProjectIssues(g.projectIDReport, &opts)
	if err != nil {
		panic(err)
	}
	if len(issues) != 1 {
		log.Fatalln("There are no open Newsletter issues. Exiting.")
	}
	return issues[0]
}

// UploadFile uploads a file to a project
func (g *GitLabHelper) UploadFile(projectID int, fname string) *gitlab.ProjectFile {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatalf("Unable to open file %s: %s", fname, err.Error())
	}
	pf, _, err := g.client.Projects.UploadFile(projectID, f, fname)
	if err != nil {
		log.Fatalf("Unable to upload graphs from file %s to project id %d: %s", fname, projectID, err.Error())
	}
	return pf
}

func filterSeverityLabels(labels []string) (filteredLabels []string) {
	// severityTest := func(s string) bool { return strings.HasPrefix(s, "S") && len(s) <= 2 }
	for _, l := range labels {
		if strings.HasPrefix(l, "severity::") {
			filteredLabels = append(filteredLabels, l)
		}
	}
	return
}

func filterServiceLabels(labels []string) (filteredLabels []string) {
	for _, l := range labels {
		if strings.HasPrefix(l, "Service::") {
			filteredLabels = append(filteredLabels, l)
		}
	}
	return
}

func filterIssuesByLabel(label string, issues []*gitlab.Issue) (filteredIssues []*gitlab.Issue) {
	for _, p := range issues {
		if stringInSlice(label, p.Labels) {
			filteredIssues = append(filteredIssues, p)
		}
	}
	return
}

func filterIssuesByState(state string, issues []*gitlab.Issue) (filteredIssues []*gitlab.Issue) {
	for _, p := range issues {
		fmt.Println(state, p.Labels)
		if p.State == state {
			filteredIssues = append(filteredIssues, p)
		}
	}
	return
}

func choose(ss []string, test func(string) bool) (ret []string) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// GetStartTimeOfPreviousShift finds the start time of the APAC shift,
// which is when the weekly handover occurs.
// This occurs at 1 AM UTC on tuesdays (+ DST).
func GetStartTimeOfPreviousShift(now time.Time) (*time.Time, error) {
	gmt, err := time.LoadLocation("Europe/London")
	if err != nil {
		return nil, err
	}

	// go back 3 days to have some flexibility for reports that are generated late
	t := now.UTC().Add(-3 * 24 * time.Hour)
	// find previous tuesday
	for t.Weekday() != time.Tuesday {
		t = t.Add(-24 * time.Hour)
	}
	// truncate to midnight UTC
	t = t.Truncate(24 * time.Hour)
	// add one hour to align with start of APAC shift
	t = t.Add(time.Hour)
	if !t.In(gmt).IsDST() {
		t = t.Add(time.Hour)
	}
	return &t, nil
}
