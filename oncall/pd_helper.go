package oncall

import (
	"time"

	pagerduty "github.com/PagerDuty/go-pagerduty"
	config "gitlab.com/gitlab-com/gl-infra/oncall-robot-assistant/config"
)

// GetOncallPersons returns the team members who were on call
// for (the last 6 days + tomorrow) for the given schedule
func getOncallPersons(config *config.Config, schedule string) []pagerduty.User {
	var options pagerduty.ListOnCallUsersOptions
	options.Since = nowPdDateWithOffset(-config.DayOffset)
	options.Until = nowPdDateWithOffset(+2)
	client := pagerduty.NewClient(config.APITokens.PagerDuty)
	if oncall, err := client.ListOnCallUsers(schedule, options); err != nil {
		panic(err)
	} else {
		return oncall
	}
}

func nowPdDateWithOffset(offset int) string {
	return time.Now().UTC().AddDate(0, 0, offset).Format("2006-01-02T15:04:05Z")
}
