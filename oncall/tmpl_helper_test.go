package oncall

import (
	"testing"
	"time"
)

func mustParseTime(str string) time.Time {
	t, err := time.Parse(time.RFC3339, str)
	if err != nil {
		panic(err)
	}
	return t
}

func TestGetStartTimeOfPreviousShift(t *testing.T) {
	tests := []struct {
		now  time.Time
		want time.Time
	}{
		{
			now:  mustParseTime("2021-10-05T13:11:05Z"),
			want: mustParseTime("2021-09-28T01:00:00Z"),
		},
		{
			now:  mustParseTime("2021-10-04T13:11:05Z"),
			want: mustParseTime("2021-09-28T01:00:00Z"),
		},
		{
			now:  mustParseTime("2021-10-25T14:12:06Z"),
			want: mustParseTime("2021-10-19T01:00:00Z"),
		},
		{
			now:  mustParseTime("2021-11-01T14:12:06Z"),
			want: mustParseTime("2021-10-26T01:00:00Z"),
		},
		{
			now:  mustParseTime("2021-11-08T14:12:06Z"),
			want: mustParseTime("2021-11-02T02:00:00Z"),
		},
	}

	for _, tt := range tests {
		got, err := GetStartTimeOfPreviousShift(tt.now)
		if err != nil {
			t.Error(err)
		}
		if *got != tt.want {
			t.Errorf("incorrect start of shift, now %v, want %v, got %v", tt.now, tt.want, got)
		}
	}
}
